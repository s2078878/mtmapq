# mtMAPQ - mtDNA Variant Calling with MAPQ

This Nextflow pipeline will call SNVs and Indels for the mitochondrial genome. It will remove potentially confounding reads introduced by germline integrations of the mitochondrial genome into the nuclear genome by comparing mapping qualities of realigned mitochondrial reads to the nuclear genome, removing any with higher mapping qualities. 

## Running the Pipeline

### 1.Cloning Repository

Clone the repository and cd into the directory using the following commands:

```shell
git clone https://git.ecdf.ed.ac.uk/s2078878/mtmapq.git
cd mtmapq
```

### 2.Create Environment

To create a conda environment with all the necessary packages, run the following command:

```shell
conda env create --file env.yml
```

Then activate the environment
```shell
conda activate mtMAPQ
```

### 3.Creating Index 

NUMTs (pronounced new-mites) are integrations of the mitochondrial genome into the nuclear DNA. This process has occurred throughout mammalian evolution, therefore mtDNA fragments are common throughout the genome. Due to NUMTs similarity to "authentic" circular mtDNA, sequencing reads originating from NUMTs can be wrongly mapped back to the mitochondrial genome. This is an issue as these additional reads can introduce false positives into the variant calling pipeline. 

To remove these potentially confounding reads, mtMAPQ extracts reads mapping to the mitochondrial genome and remaps them back to the nuclear DNA. If the reads are capable of mapping to the nuclear genome with a greater mapping quality than that of the mitochondrial reference, they are considered reads originating from NUMTs and are therefore excluded from variant calling. Due to this remapping stage, a reference genome containing only the nuclear chromosomes is required. This can be created for the hg38 reference genome by running the following commands:

```shell
mkdir data/genome/
wget -P data/genome/ 'ftp://hgdownload.cse.ucsc.edu/goldenPath/hg38/chromosomes/*.fa.gz'
rm data/genome/chrM.fa.gz
gunzip data/genome/*.fa.gz
cat data/genome/*.fa > data/genome/hg38.fa
rm data/genome/chr*.fa
```

### 4.Make Support Files

The Nextflow pipeline works from a .csv file containing the main parameters needed. The csv file should have the following format and column names:

| row | sampleID | group  | bam                          | index                        |
|:----|:---------|:-------|:-----------------------------|:-----------------------------|
| 1   | sample1  | normal | path/to/sample/alignment.bam | path/to/sample/index.bam.bai |
| 2   | sample1  | tumour | path/to/sample/alignment.bam | path/to/sample/index.bam.bai |
| 3   | sample2  | normal | path/to/sample/alignment.bam | path/to/sample/index.bam.bai |
| 4   | sample2  | tumour | path/to/sample/alignment.bam | path/to/sample/index.bam.bai |

Alignment and index files can be for the entire genome, there is no need to subset to only the mitochondrial DNA.

In addition to this a sample purity file can also be supplied. This should be in the below format and should be a .csv file. sampleIDs should match those in the parameters file. A purity of 1 would indicate a completely pure sample. 

| row | sampleID | group  | purity |
|:----|:---------|:-------|:-------|
| 1   | sample1  | normal | 0.992  |
| 2   | sample1  | tumour | 0.995  |
| 3   | sample2  | normal | 0.997  |
| 4   | sample2  | tumour | 0.986  |

If purity data is not available, do not set the purity parameter when running mtMAPQ. 

### 5.Run mtMAPQ

Everything should now be ready to run mtMAPQ. A typical command will look something similar to the one below, substituting in paths to your own parameter, purity and reference files. 

```shell
nextflow run mtMAPQ.nf \
  --parameters path/to/parameters/file.csv \ # parameters file (see step 4)
  --reference data/genome/hg38.fa \ # nuclear genome file made in step 3
  --purity /path/to/purity/file.csv \ # purity data, exlude if no purity data available
  --outdir path/to/out/directory/ # where should the results folder be created?
```

This will output a results directory containing somatic and germline variant calls. 

Files beginning with "combined" are the raw unfiltered variant calls. These files are likely to contain many false positives. 

Files beginning with "somatic" are the somatic variant calls. These are the filtered somatic variant calls down to a variant allele frequency of 0.25%. Both SNV and Indel call files are provided.

Files beginning with "germline" are the germline variant calls. These are the filtered germline variant calls down to a variant allele frequency of 0.25%. Both SNV and Indel call files are provided.


