###########################################
# This script filters read quality scores #
# to those higher in mitochondria than in #
# autosomes                               #
############################################

library(data.table)
library(dplyr)

# load the command line arguments
args = commandArgs(trailingOnly=T)

auto <- fread(args[1])
mito <- fread(args[2])
out <- args[3]

merged <- left_join(auto, mito, by = c("V1", "V2"))

filt <- filter(merged, V3.x > V3.y | V3.x == V3.y)

final <- filt[,1]

fwrite(final, out, sep = "\t", quote = F, row.names = F, col.names = F)

