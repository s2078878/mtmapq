#!/usr/bin/env nextflow

log.info """\
         =========================================================
         M T M A P Q :   M T D N A   V A R I A N T   C A L L I N G
         =========================================================
         parameters:    ${params.parameters}
         reference:     ${params.reference}
         purity:        ${params.purity}
         outdir:        ${params.outdir}
         =========================================================
         """
         .stripIndent()

/*
 * Input parameters validation
 */
if (params.parameters){
    
    parameters_file = file(params.parameters)

    Channel
        .fromPath(params.parameters)
        .splitCsv(header:true)
        .map{ row -> tuple(row.row, row.sampleID, row.group, row.bam, row.index)}
        .set{ parameters_ch }
    
    if( !parameters_file.exists() ) exit 1, "ERROR: Parameters file doesn't exist: ${parameters_file}"

} else {
    
    exit 1, "ERROR: Missing parameters file. Please set a parameters file using the --parameters flag."

}

if (params.reference){

    reference_file = file(params.reference)

    if( !reference_file.exists() ) exit 1, "ERROR: Reference file doesn't exist: ${reference_file}"

} else {
    
    exit 1, "ERROR: Missing reference index file. Please set a index file using the --reference flag."

}

if (params.purity){
    
    purity_file = file(params.purity)

    Channel
        .fromPath(params.purity)
        .splitCsv(header:true)
        .map{ row -> tuple(row.row, row.sampleID, row.group, row.purity) }
        .set{ purity_ch }
    
    if( !purity_file.exists() ) exit 1, "Purity file doesn't exist: ${purity_file}"

} else {
    
    purity_file = "false"

}

/*
 * Create index files for alignment
 */
process index {
    tag "Creating index files"

    input:
    file(reference_file)

    output:
    path("index.*") into index_ch

    script:
    """
    bwa index -p index -a bwtsw ${reference_file}
    """
}

/*
 * Extract only the mitochondria reads from the bam files
 */
process chrm {
    tag "Pulling chrM"

    input:
    set row, sampleID, group, path(bam), path(index) from parameters_ch

    output:
    set sampleID, group, path("${sampleID}.${group}.mito.bam"), path("${sampleID}.${group}.mito.bam.bai")into mitochondria_ch
    set sampleID, group, path("${sampleID}.${group}.mito.bam") into mitochondria_ch2, mitochondria_ch3

    script:
    """
    samtools view -hb ${bam} chrM > ${sampleID}.${group}.mito.bam
    samtools index ${sampleID}.${group}.mito.bam
    """
}

/*
 * Subset the BAM file to the target regions and output reads
 */
process subset {
    tag "Subsetting to fastq files"

    input:
    set sampleID, group, path(bam), path(index) from mitochondria_ch

    output:
    set sampleID, group, path("${sampleID}.${group}.R1.fastq.gz"), path("${sampleID}.${group}.R2.fastq.gz") into subset_ch

    script:
    """
    cp $baseDir/data/chrM.bed .
    bazam -bam ${bam} -L chrM.bed -pad 0 -r1 ${sampleID}.${group}.R1.fastq -r2 ${sampleID}.${group}.R2.fastq 
    gzip *.fastq
    """
}

/*
 * Realign the extracted fastq files back to the autosomes
 */
process align {
    tag "Realigning reads"

    input:
    set sampleID, group, path(read1), path(read2) from subset_ch
    path(index) from index_ch

    output:
    set sampleID, group, path("${sampleID}.${group}.auto.bam") into realigned_ch
    
    script:
    """
    bwa mem index ${read1} ${read2} | samtools view -hb - > ${sampleID}.bam 
    samtools sort ${sampleID}.bam > ${sampleID}.${group}.auto.bam
    """
}

/*
 * Merge bam files into single channel
 */
realigned_ch
    .mix(mitochondria_ch2)
    .groupTuple(by:[0,1]) 
    .map { input -> tuple(input[0], input[1], input[2][0], input[2][1]) }
    .set { merged_ch }

/*
 * Get all read IDs and their mapping quality
 */
process ids {
    tag "Getting IDs and mapping qualities"

    input:
    set sampleID, group, path(mito_bam), path(auto_bam) from merged_ch

    output:
    set sampleID, group, path("${sampleID}.mito_ids.txt"), path("${sampleID}.auto_ids.txt") into ids_ch

    script:
    """
    samtools index ${mito_bam}
    samtools index ${auto_bam}

    cp $baseDir/scripts/higherQuality.py .
    python higherQuality.py ${mito_bam} ${sampleID}.mito_ids.txt
    python higherQuality.py ${auto_bam} ${sampleID}.auto_ids.txt
    """
}

/*
 * Filter the reads for those with higher quality
 */
process filter_reads {
    tag "Identifying NUMT reads"

    input:
    set sampleID, group, path(mito_ids), path(auto_ids) from ids_ch

    output:
    set sampleID, group, path("${sampleID}.${group}.txt") into filtIDs_ch

    script:
    """
    cp $baseDir/scripts/compareQuality.R .
    Rscript compareQuality.R ${auto_ids} ${mito_ids} ${sampleID}.${group}.txt
    """
}

/*
 * Merge original bam file and ids to be removed into single channel
 */
filtIDs_ch
    .mix(mitochondria_ch3)
    .groupTuple(by:[0,1]) 
    .map { input -> tuple(input[0], input[1], input[2][0], input[2][1]) }
    .set { remove_ch }

/*
 * Filter the original mitochondria file 
 */
process filter_bam {
    tag "Identifying NUMT reads"

    input:
    set sampleID, group, path(bam_file), path(remove_ids) from remove_ch

    output:
    set sampleID, group, path("${sampleID}.${group}.filt.bam") into filt_ch

    script:
    """
    picard FilterSamReads I=${bam_file} O=${sampleID}.${group}.filt.bam \
        READ_LIST_FILE=${remove_ids} FILTER=excludeReadList
    """
}

/*
 * Sort the files and create mpileup file ready for varscan
 */
process pileup {
    tag "Sorting and creating pileup files"

    input:
    set sampleID, group, path(filtered_chrM) from filt_ch

    output:
    set sampleID, group, path("${sampleID}.${group}.pileup") into mpileup_ch

    script:
    """
    samtools sort ${filtered_chrM} | \
    samtools mpileup -d 0 -f $baseDir/data/chrM.fa - > "${sampleID}.${group}.pileup"
    """
}

if(purity_file != "false"){

    // Merge the pileup files and the purity data
    mpileup_ch
        .mix(purity_ch)
        .groupTuple(by:[0,1]) 
        .map { input -> tuple(input[0], input[2][0], input[2][1]) }
        .groupTuple(by:0)
        .map { input -> tuple(input[0], input[1][0], input[1][1], input[2][0], input[2][1]) }
        .set { merged_pileup_ch }

} else {

    mpileup_ch
        .groupTuple(by:0)
        .map { sampleID, groups, files -> tuple( sampleID, groups, files.sort{it.name} ) }
        .map { input -> tuple(input[0], 1, 1, input[2][0], input[2][1]) }
        .set { merged_pileup_ch }

}

/*
 * Run VarScan2 to call mitochondrial DNA variants and if they are germline/somatic
 */
process varscan {
    tag "Running VarScan2"

    input:
    set sampleID, n_purity, t_purity, path(n_pileup), path(t_pileup) from merged_pileup_ch

    output:
    set path("${sampleID}.snp"), path("${sampleID}.indel") into var_ch

    script:
    """
    varscan somatic ${n_pileup} ${t_pileup} ${sampleID} \
        --strand-filter 1 --min-avg-qual 30 --min-coverage 2 --min-reads 2 --min-var-freq 0 \
        --normal-purity ${n_purity} --tumor-purity ${t_purity}
    """
}

//     // these will probably be the final parameters
//     // varscan somatic ${n_pileup} ${t_pileup} ${sampleID} \
//     // --strand-filter 1 --min-avg-qual 30 --min-coverage 2 --min-reads 20 --min-var-freq 0.0025 \
//     // --normal-purity ${n_purity} --tumor-purity ${t_purity} --somatic-p-value 0.001

/*
 * Merge all variant calls
 */
process merge {
    tag "Merging variant calls"
    publishDir "${params.outdir}/results/" , mode:'copy'

    input:
    path(var_calls) from var_ch.collect()

    output:
    set path("combinedSNPs.txt"), path("combinedIndels.txt") into combined_ch

    script:
    """
    cp $baseDir/scripts/mergeCalls.R .
    Rscript mergeCalls.R ${var_calls}
    """
}

/*
 * Filter the variant calls
 */
process filter {
    tag "Filtering variant calls"
    publishDir "${params.outdir}/results/" , mode:'copy'

    input:
    set path(snps), path(indels) from combined_ch

    output:
    set path("somaticSNVs.txt"), path("somaticIndels.txt"), path("germlineSNVs.txt"), path("germlineIndels.txt") into filtered_ch

    script:
    """
    cp $baseDir/scripts/filterCalls.R .
    Rscript filterCalls.R ${snps} ${indels}
    """
}

workflow.onComplete {
	log.info ( workflow.success ? "\nDone! All mitochondrial DNA variants called.\n" : "\nOops .. something went wrong.\n" )
}